# ZoomVenues - Front-End Developer Exercise

## Instructions

You are building a list of event team members using Angular JS or basic HTML, CSS and JS.

![alt text](https://raw.githubusercontent.com/lennd/front-end-developer-exercise/master/excercise-design.png "Exercise")

#### Details
- Use the JPG contained in this repository as your visual guide. 
- You must consume this endpoint (http://jsonplaceholder.typicode.com/users) to get the users for the list. How you consume the endpoint is up to you.
- You can use static CSS, setup a CSS precompiler + watching.
- You should be able to filter the list by typing into the filter text input
- Clicking on a row should expand it to reveal the user's address -- only one row should be able to be expanded at a time
- You should demonstrate knowledge on how to construct and re-use components

#### Bonus points
- Use ES6
- Write tests for your components
- Demonstrate knowledge of pure functions / components

#### For fun / if you want to get crazy
- Add animation to the expanding rows
- Output some location-relative information using the latitude / longitude returned by the API endpoint for each user
- Add a "loading" indicator for when the users are being fetched
- Use a Gravatar for displaying a user's avatar based on the user's email

#### To submit
- Send Robin (robin@zoomvenues.com) an email with a link to your repository

#### Notes
- The design shows just a sampling of users. You should display all of the users that the endpoint returns.
- For the icons, use FontAwesome (https://fortawesome.github.io/Font-Awesome/icons/). The CSS has already been included in the project.

#### Questions / problems / comments?
Contact Robin - robin@zoomvenues.com

---
